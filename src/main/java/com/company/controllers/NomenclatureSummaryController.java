package com.company.controllers;

import com.company.model.NomenclatureSummary;
import com.company.services.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureSummaryController {

    private final NomenclatureSummaryService nomenclatureSummaryServices;

    public NomenclatureSummaryController(NomenclatureSummaryService nomenclatureSummaryServices) {
        this.nomenclatureSummaryServices = nomenclatureSummaryServices;
    }

    @GetMapping("/NomenclatureSummaries")
    public ResponseEntity<?> getNomenclatureSummaries(){
        return ResponseEntity.ok ( nomenclatureSummaryServices.getAll () );
    }

    @GetMapping("/NomenclatureSummary/{nomenclatureSummaryId}")
    public ResponseEntity<?> getNomenclatureSummary(@PathVariable Long nomenclatureSummaryId){
        return ResponseEntity.ok ( nomenclatureSummaryServices.getById ( nomenclatureSummaryId ) );
    }

    @PostMapping("/NomenclatureSummary")
    public ResponseEntity<?> saveNomenclatureSummary(@RequestBody NomenclatureSummary nomenclatureSummary){
        return ResponseEntity.ok ( nomenclatureSummaryServices.create ( nomenclatureSummary ) );
    }

    @PutMapping("/NomenclatureSummary")
    public ResponseEntity<?> updateNomenclatureSummary(@RequestBody NomenclatureSummary nomenclatureSummary){
        return ResponseEntity.ok ( nomenclatureSummaryServices.update ( nomenclatureSummary ) );
    }

    @DeleteMapping("/NomenclatureSummary/{nomenclatureSummaryId}")
    public void delete(@PathVariable Long nomenclatureSummaryId){
        nomenclatureSummaryServices.delete ( nomenclatureSummaryId );
    }
}
