package com.company.controllers;

import com.company.model.SearchKeyRouting;
import com.company.services.SearchKeyRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyRoutingController {

    private final SearchKeyRoutingService searchKeyRoutingServices;

    public SearchKeyRoutingController(SearchKeyRoutingService searchKeyRoutingServices) {
        this.searchKeyRoutingServices = searchKeyRoutingServices;
    }

    @GetMapping("/SearchKeysRouting")
    public ResponseEntity<?> getSearchKeysRouting(){
        return ResponseEntity.ok ( searchKeyRoutingServices.getAll () );
    }

    @GetMapping("/SearchKeyRouting/{searchKeyRoutingId}")
    public ResponseEntity<?> getSearchKeyRouting(@PathVariable Long searchKeyRoutingId){
        return ResponseEntity.ok ( searchKeyRoutingServices.getById ( searchKeyRoutingId ) );
    }

    @PostMapping("/SearchKeyRouting")
    public ResponseEntity<?> saveSearchKeyRouting(@RequestBody SearchKeyRouting searchKeyRouting){
        return ResponseEntity.ok ( searchKeyRoutingServices.create ( searchKeyRouting ) );
    }

    @PutMapping("/SearchKeyRouting")
    public ResponseEntity<?> updateSearchKeyRouting(@RequestBody SearchKeyRouting searchKeyRouting){
        return ResponseEntity.ok ( searchKeyRoutingServices.update ( searchKeyRouting ) );
    }

    @DeleteMapping("/SearchKeyRouting/{searchKeyRoutingId}")
    public void deleteSearchKeyRouting(@PathVariable Long searchKeyRoutingId){
        searchKeyRoutingServices.delete ( searchKeyRoutingId );
    }
}
