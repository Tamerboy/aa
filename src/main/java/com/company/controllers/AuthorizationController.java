package com.company.controllers;

import com.company.model.Authorization;
import com.company.services.AuthorizationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthorizationController {

    private final AuthorizationService authorizationServices;

    public AuthorizationController(AuthorizationService authorizationServices) {
        this.authorizationServices = authorizationServices;
    }


    @GetMapping("/Authorizations")
    public ResponseEntity<?> getAuthorizations(){
        return ResponseEntity.ok ( authorizationServices.getAll ());
    }

    @GetMapping("/Authorization/{authorizationId}")
    public ResponseEntity<?> getAuthorization(@PathVariable Long authorizationId ){
        return ResponseEntity.ok ( authorizationServices.getById ( authorizationId ) );
    }

    @PostMapping("/Authorization")
    public ResponseEntity<?> saveAuthorization(@RequestBody Authorization authorization){
        return ResponseEntity.ok ( authorizationServices.create ( authorization ) );
    }

    @PutMapping("/Authorization")
    public ResponseEntity<?> updateAuthorization(@RequestBody Authorization authorization){
        return ResponseEntity.ok ( authorizationServices.update ( authorization ) );
    }

    @DeleteMapping("/Authorization/{authorizationId}")
    public void deleteAuthorization(@PathVariable Long authorizationId){
          authorizationServices.delete ( authorizationId );
    }

}
