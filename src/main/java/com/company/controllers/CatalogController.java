package com.company.controllers;

import com.company.model.Catalog;
import com.company.services.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogController {

    private final CatalogService catalogServices;

    public CatalogController(CatalogService catalogServices) {
        this.catalogServices = catalogServices;
    }

   @GetMapping("/Catalogs")
    public ResponseEntity<?> getCatalogs(){
        return ResponseEntity.ok ( catalogServices.getAll () );
    }

    @GetMapping("/Catalog/{catalogId}")
    public ResponseEntity<?> getCatalog(@PathVariable Long catalogId){
        return ResponseEntity.ok ( catalogServices.getById ( catalogId ) );
    }

    @PostMapping("/Catalog")
    public ResponseEntity<?> saveCatalog(@RequestBody Catalog catalog){
        return  ResponseEntity.ok ( catalogServices.create ( catalog ) );
    }

    @PutMapping("/Catalog")
    public ResponseEntity<?> updateCatalog(@RequestBody Catalog catalog){
        return  ResponseEntity.ok ( catalogServices.update ( catalog ) );
    }

    @DeleteMapping("/Catalog/{catalogId}")
    public void deleteCatalog(@PathVariable Long catalogId){
        catalogServices.delete ( catalogId );
    }

}
