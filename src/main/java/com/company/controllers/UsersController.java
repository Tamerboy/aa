package com.company.controllers;

import com.company.model.Users;
import com.company.services.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsersController {

    private final UsersService usersServices;

    public UsersController(UsersService usersServices) {
        this.usersServices = usersServices;
    }

    @GetMapping("/Users")
    public ResponseEntity<?> getUsers(){
        return ResponseEntity.ok ( usersServices.getAll () );
    }

    @GetMapping("/User/{usersId}")
    public ResponseEntity<?> getUser(@PathVariable Long usersId){
        return ResponseEntity.ok ( usersServices.getById ( usersId ) );
    }

    @PostMapping("/Users")
    public ResponseEntity<?> saveUser(@RequestBody Users users){
        return ResponseEntity.ok ( usersServices.create ( users ) );
    }

    @PutMapping("/Users")
    public ResponseEntity<?> updateUser(@RequestBody Users users){
        return ResponseEntity.ok ( usersServices.update ( users ) );
    }

    @DeleteMapping("/Users/{usersId}")
    public void deleteUser(@PathVariable Long usersId){
        usersServices.delete ( usersId );
    }




}
