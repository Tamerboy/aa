package com.company.controllers;

import com.company.model.CatalogCase;
import com.company.services.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogCaseController {

    private final CatalogCaseService catalogCaseServices;

    public CatalogCaseController(CatalogCaseService catalogCaseServices) {
        this.catalogCaseServices = catalogCaseServices;
    }

    @GetMapping("/CatalogCases")
    public ResponseEntity<?> getCatalogCases(){
        return ResponseEntity.ok ( catalogCaseServices.getAll () );
    }

    @GetMapping("/CatalogCase/{catalogCaseId}")
    public ResponseEntity<?> getCatalogCase(@PathVariable Long catalogCaseId ){
        return ResponseEntity.ok ( catalogCaseId );
    }

    @PostMapping("/CatalogCase")
    public ResponseEntity<?> saveCatalogCase(@RequestBody CatalogCase catalogCase){
        return ResponseEntity.ok ( catalogCaseServices.create ( catalogCase ) );
    }

    @PutMapping("/CatalogCase")
    public ResponseEntity<?> updateCatalogCase(@RequestBody CatalogCase catalogCase){
        return ResponseEntity.ok ( catalogCaseServices.update ( catalogCase ) );
    }

    @DeleteMapping("/CatalogCase/{catalogCaseId}")
    public void deleteCatalogCase(@PathVariable Long catalogCaseId){
        catalogCaseServices.delete ( catalogCaseId );
    }

}
