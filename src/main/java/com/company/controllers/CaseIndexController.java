package com.company.controllers;

import com.company.model.CaseIndex;
import com.company.services.CaseIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseIndexController {

    private final CaseIndexService caseIndexServices;

    public CaseIndexController(CaseIndexService caseIndexServices) {
        this.caseIndexServices = caseIndexServices;
    }

    @GetMapping("/CaseIndexes")
    public ResponseEntity<?> getCaseIndexes(){
        return ResponseEntity.ok ( caseIndexServices.getAll () );
    }

    @GetMapping("/CaseIndex/{caseIndexId}")
    public ResponseEntity<?> getCaseIndex(@PathVariable Long caseIndexId){
        return ResponseEntity.ok ( caseIndexServices.getById ( caseIndexId ) );
    }

    @PostMapping("/CaseIndex")
    public ResponseEntity<?> saveCaseIndex(@RequestBody CaseIndex caseIndex){
        return ResponseEntity.ok ( caseIndexServices.create ( caseIndex ) );
    }

    @PutMapping("/CaseIndex")
    public ResponseEntity<?> updateCaseIndex(@RequestBody CaseIndex caseIndex){
        return ResponseEntity.ok ( caseIndexServices.update ( caseIndex ) );
    }

    @DeleteMapping("/CaseIndex/{caseIndexId}")
    public void delete(@PathVariable Long caseIndexId){
        caseIndexServices.delete ( caseIndexId );
    }
}
