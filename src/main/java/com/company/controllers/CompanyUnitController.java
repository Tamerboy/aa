package com.company.controllers;

import com.company.model.CompanyUnit;
import com.company.services.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyUnitController {

    private final CompanyUnitService companyUnitServices;

    public CompanyUnitController(CompanyUnitService companyUnitServices) {
        this.companyUnitServices = companyUnitServices;
    }

    @GetMapping("/CompanyUnits")
    public ResponseEntity<?> getCompanyUnits(){
        return ResponseEntity.ok ( companyUnitServices.getAll () );
    }

    @GetMapping("/CompanyUnit/{companyUnitId}")
    public ResponseEntity<?> getCompanyUnit(@PathVariable Long companyUnitId){
        return ResponseEntity.ok ( companyUnitServices.getById ( companyUnitId ) );
    }

    @PostMapping("/CompanyUnit")
    public ResponseEntity<?> saveCompanyUnit(@RequestBody CompanyUnit companyUnit){
        return ResponseEntity.ok ( companyUnitServices.create ( companyUnit ) );
    }

    @PutMapping("/CompanyUnit")
    public ResponseEntity<?> updateCompanyUnit(@RequestBody CompanyUnit companyUnit){
        return ResponseEntity.ok ( companyUnitServices.update ( companyUnit ) );
    }

    @DeleteMapping("/CompanyUnit/{companyUnitId}")
    public void deleteCompanyUnit(@PathVariable Long companyUnitId){
        companyUnitServices.delete ( companyUnitId );
    }



}
