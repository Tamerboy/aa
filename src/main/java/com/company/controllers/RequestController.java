package com.company.controllers;

import com.company.model.Request;
import com.company.services.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestController {

    private final RequestService requestServices;

    public RequestController(RequestService requestServices) {
        this.requestServices = requestServices;
    }

    @GetMapping("/Requests")
    public ResponseEntity<?> getRequests(){
        return ResponseEntity.ok ( requestServices.getAll () );
    }

    @GetMapping("/Request/{requestId}")
    public ResponseEntity<?> getRequest(@PathVariable Long requestId){
        return ResponseEntity.ok ( requestServices.getById ( requestId ) );
    }

    @PostMapping("/Request")
    public ResponseEntity<?> saveRequest(@RequestBody Request request){
        return ResponseEntity.ok ( requestServices.create ( request ) );
    }

    @PutMapping("/Request")
    public ResponseEntity<?> updateRequest(@RequestBody Request request){
        return ResponseEntity.ok ( requestServices.update ( request ) );
    }

    @DeleteMapping("/Request/{requestId}")
    public void deleteRequest(@PathVariable Long requestId){
        requestServices.delete ( requestId );
    }
}
