package com.company.controllers;

import com.company.model.Nomenclature;
import com.company.services.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureController {

    private final NomenclatureService nomenclatureServices;

    public NomenclatureController(NomenclatureService nomenclatureServices) {
        this.nomenclatureServices = nomenclatureServices;
    }

    @GetMapping("/Nomenclatures")
    public ResponseEntity<?> getNomenclatures(){
        return ResponseEntity.ok ( nomenclatureServices.getAll () );
    }

    @GetMapping("/Nomenclature/{nomenclatureId}")
    public ResponseEntity<?> getNomenclature(@PathVariable Long nomenclatureId){
        return ResponseEntity.ok ( nomenclatureServices.getById ( nomenclatureId ) );
    }

    @PostMapping("/Nomenclature")
    public ResponseEntity<?> saveNomenclature(@RequestBody Nomenclature nomenclature){
        return ResponseEntity.ok ( nomenclatureServices.create ( nomenclature ) );
    }

    @PutMapping("/Nomenclature")
    public ResponseEntity<?> updateNomenclature(@RequestBody Nomenclature nomenclature){
        return ResponseEntity.ok ( nomenclatureServices.update ( nomenclature ) );
    }

    @DeleteMapping("/Nomenclature/{nomenclatureId}")
    public void deleteNomenclature(@PathVariable Long nomenclatureId){
        nomenclatureServices.delete ( nomenclatureId );
    }
}
