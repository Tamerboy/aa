package com.company.controllers;


import com.company.model.ActivityJournal;
import com.company.services.ActivityJournalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityJournalController {

    private final ActivityJournalService activityJournalServices;

    public ActivityJournalController(ActivityJournalService activityJournalServices) {
        this.activityJournalServices = activityJournalServices;
    }

    @GetMapping("/ActivityJournals")
    public ResponseEntity<?> getActivityJournals(){
        return ResponseEntity.ok ( activityJournalServices.getAll () );
    }

    @GetMapping("/ActivityJournal/activityJournalId")
    public ResponseEntity<?> getActivityJournal(@PathVariable Long activityJournalId ){
        return ResponseEntity.ok ( activityJournalServices.getById ( activityJournalId ) );
    }

    @PostMapping("/ActivityJournal")
    public ResponseEntity<?> saveActivityJournal(@RequestBody ActivityJournal activityJournal){
        return ResponseEntity.ok ( activityJournalServices.create ( activityJournal ) );
    }

    @PutMapping("/ActivityJournal")
    public ResponseEntity<?> updateAuthorization(@RequestBody ActivityJournal activityJournal){
        return ResponseEntity.ok ( activityJournalServices.update ( activityJournal ) );
    }

    @DeleteMapping
    public void deleteAuthorization(@PathVariable Long id){
        activityJournalServices.delete ( id );
    }
}
