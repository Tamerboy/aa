package com.company.controllers;

import com.company.model.Fond;
import com.company.services.FondService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FondController {

    private final FondService fondServices;

    public FondController(FondService fondServices) {
        this.fondServices = fondServices;
    }

    @GetMapping("/Fonds")
    public ResponseEntity<?> getFonds(){
        return ResponseEntity.ok ( fondServices.getAll () );
    }

    @GetMapping("/Fond/{fondId}")
    public ResponseEntity<?> getFond(@PathVariable Long fondId){
        return ResponseEntity.ok ( fondServices.getById ( fondId ) );
    }

    @PostMapping("/Fond")
    public ResponseEntity<?> saveFond(@RequestBody Fond fond){
        return ResponseEntity.ok ( fondServices.create ( fond ) );
    }

    @PutMapping("/Fond")
    public ResponseEntity<?> updateFond(@RequestBody Fond fond){
        return ResponseEntity.ok ( fondServices.update ( fond ) );
    }

    @DeleteMapping("/Fond/{fondId}")
    public void deleteFond(@PathVariable Long fondId){
        fondServices.delete ( fondId );
    }
}
