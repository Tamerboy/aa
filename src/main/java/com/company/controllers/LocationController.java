package com.company.controllers;

import com.company.model.Location;
import com.company.services.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationController {

    private final LocationService locationServices;

    public LocationController(LocationService locationServices) {
        this.locationServices = locationServices;
    }

    @GetMapping("/Locations")
    public ResponseEntity<?> getLocations(){
        return ResponseEntity.ok ( locationServices.getAll () );
    }

    @GetMapping("/Location/{locationId}")
    public ResponseEntity<?> getLocation(@PathVariable Long locationId){
        return ResponseEntity.ok ( locationServices.getById ( locationId ) );
    }

    @PostMapping("/Location")
    public ResponseEntity<?> saveLocation(@RequestBody Location location){
        return ResponseEntity.ok ( locationServices.create ( location ) );
    }

    @PutMapping("/Location")
    public ResponseEntity<?> updateLocation(@RequestBody Location location){
        return ResponseEntity.ok ( locationServices.update ( location ) );
    }

    @DeleteMapping("/Location/{locationId}")
    public void deleteLocation(@PathVariable Long locationId){
        locationServices.delete ( locationId );
    }
}
