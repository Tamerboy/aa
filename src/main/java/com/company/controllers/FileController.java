package com.company.controllers;

import com.company.model.File;
import com.company.services.FileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileController {

    private final FileService fileServices;

    public FileController(FileService fileServices) {
        this.fileServices = fileServices;
    }

    @GetMapping("/Files")
    public ResponseEntity<?> GetFiles(){
        return ResponseEntity.ok ( fileServices.getAll () );
    }

    @GetMapping("/File/{fileId}")
    public ResponseEntity<?> GetFile(@PathVariable Long fileId){
        return ResponseEntity.ok ( fileServices.getById ( fileId ) );
    }

    @PostMapping("/File")
    public ResponseEntity<?> saveFile(@RequestBody File file){
        return ResponseEntity.ok ( fileServices.create ( file ) );
    }

    @PutMapping("/File")
    public ResponseEntity<?> updateFile(@RequestBody File file){
        return ResponseEntity.ok ( fileServices.update ( file ) );
    }

    @DeleteMapping("/File/fileId")
    public void deleteFile(@PathVariable Long fileId){
        fileServices.delete ( fileId );
    }
}
