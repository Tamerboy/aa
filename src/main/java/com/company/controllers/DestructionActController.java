package com.company.controllers;

import com.company.model.DestructionAct;
import com.company.services.DestructionActService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DestructionActController {

    private final DestructionActService destructionActServices;

    public DestructionActController(DestructionActService destructionActServices) {
        this.destructionActServices = destructionActServices;
    }

    @GetMapping("/DestructionActs")
    public ResponseEntity<?> getDestructionActs(){
        return ResponseEntity.ok ( destructionActServices.getAll () );
    }

    @GetMapping("/DestructionAct/{destructionActId}")
    public ResponseEntity<?> GetDestructionAct(@PathVariable Long destructionActId){
        return ResponseEntity.ok ( destructionActServices.getById ( destructionActId ) );
    }

    @PostMapping("/DestructionAct")
    public ResponseEntity<?> saveDestructionAct(@RequestBody DestructionAct destructionAct){
        return ResponseEntity.ok ( destructionActServices.create ( destructionAct ) );
    }

    @PutMapping("/DestructionAct")
    public ResponseEntity<?> updateDestructionAct(@RequestBody DestructionAct destructionAct){
        return ResponseEntity.ok ( destructionActServices.update ( destructionAct ) );
    }

    @DeleteMapping("/DestructionAct/{destructionActId}")
    public void deleteDestructionAct(@PathVariable Long destructionActId){
        destructionActServices.delete ( destructionActId );
    }


}
