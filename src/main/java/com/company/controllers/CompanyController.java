package com.company.controllers;

import com.company.model.Company;
import com.company.services.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyController {

    private final CompanyService companyServices;

    public CompanyController(CompanyService companyServices) {
        this.companyServices = companyServices;
    }

    @GetMapping("/Companies")
    public ResponseEntity<?> getCompanies(){
        return ResponseEntity.ok ( companyServices.getAll () );
    }

    @GetMapping("/Company/{companyId}")
    public ResponseEntity<?> getCompany(@PathVariable Long companyId){
        return ResponseEntity.ok ( companyServices.getById ( companyId ) );
    }

    @PostMapping("/Company")
    public ResponseEntity<?> saveCompany(@RequestBody Company company){
        return ResponseEntity.ok ( companyServices.create ( company ) );
    }

    @PutMapping("/Company")
    public ResponseEntity<?> updateCompany(@RequestBody Company company){
        return ResponseEntity.ok ( companyServices.update ( company ) );
    }

    @DeleteMapping("/Company/{companyId}")
    public void deleteCompany(@PathVariable Long companyId){
        companyServices.delete ( companyId );
    }
}
