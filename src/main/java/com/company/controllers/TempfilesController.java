package com.company.controllers;

import com.company.model.Tempfiles;
import com.company.services.TempfilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TempfilesController {

    private final TempfilesService tempfilesServices;

    public TempfilesController(TempfilesService tempfilesServices) {
        this.tempfilesServices = tempfilesServices;
    }

    @GetMapping("/Tempfiles")
    public ResponseEntity<?> getTempfiles(){
        return ResponseEntity.ok ( tempfilesServices.getAll () );
    }

    @GetMapping("/Tempfile/{tempfilesId}")
    public ResponseEntity<?> getTempfile(@PathVariable Long tempfilesId){
        return ResponseEntity.ok (tempfilesServices.getById ( tempfilesId ) );
    }

    @PostMapping("/Tempfiles")
    public ResponseEntity<?> saveTempfiles(@RequestBody Tempfiles tempfiles){
        return ResponseEntity.ok ( tempfilesServices.create ( tempfiles ) );
    }

    @PutMapping("/Tempfiles")
    public ResponseEntity<?> updateTempfiles(@RequestBody Tempfiles tempfiles){
        return ResponseEntity.ok ( tempfilesServices.update ( tempfiles ) );
    }

    @DeleteMapping("/Tempfiles/{tempfilesId}")
    public void deleteTempfiles(@PathVariable Long tempfilesId){
        tempfilesServices.delete ( tempfilesId );
    }
}
