package com.company.controllers;

import com.company.model.SearchKey;
import com.company.services.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyController {

    private final SearchKeyService searchKeyServices;

    public SearchKeyController(SearchKeyService searchKeyServices) {
        this.searchKeyServices = searchKeyServices;
    }

    @GetMapping("/SearchKeys")
    public ResponseEntity<?> getSearchKeys(){
        return ResponseEntity.ok ( searchKeyServices.getAll () );
    }

    @GetMapping("/SearchKey/{searchKeyId}")
    public ResponseEntity<?> getSearchKey(@PathVariable Long searchKeyId){
        return ResponseEntity.ok ( searchKeyServices.getById ( searchKeyId ) );
    }

    @PostMapping("/SearchKey")
    public ResponseEntity<?> saveSearchKey(@RequestBody SearchKey searchKey){
        return ResponseEntity.ok ( searchKeyServices.create ( searchKey ) );
    }

    @PutMapping("/SearchKey")
    public ResponseEntity<?> updateSearchKey(@RequestBody SearchKey searchKey){
        return ResponseEntity.ok ( searchKeyServices.update ( searchKey ) );
    }

    @DeleteMapping("/SearchKey/{searchKeyId}")
    public void deleteSearchKey(@PathVariable Long searchKeyId){
        searchKeyServices.delete ( searchKeyId );
    }
}
