package com.company.controllers;

import com.company.model.Share;
import com.company.services.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShareController {

    private final ShareService shareServices;

    public ShareController(ShareService shareServices) {
        this.shareServices = shareServices;
    }

    @GetMapping("/Shares")
    public ResponseEntity<?> getShares(){
        return ResponseEntity.ok ( shareServices.getAll () );
    }

    @GetMapping("/Share/{shareId}")
    public ResponseEntity<?> getShare(@PathVariable Long shareId){
        return ResponseEntity.ok ( shareServices.getById ( shareId ) );
    }

    @PostMapping("/Share")
    public ResponseEntity<?> saveShare(@RequestBody Share share){
        return ResponseEntity.ok ( shareServices.create ( share ) );
    }

    @PutMapping("/Share")
    public ResponseEntity<?> updateShare(@RequestBody Share share){
        return ResponseEntity.ok ( shareServices.update ( share ) );
    }

    @DeleteMapping("/Share/{shareId}")
    public void delete(@PathVariable Long shareId){
        shareServices.delete ( shareId );
    }
}
