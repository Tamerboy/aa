package com.company.controllers;

import com.company.model.Notification;
import com.company.services.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotificationController {

    private final NotificationService notificationServices;

    public NotificationController(NotificationService notificationServices) {
        this.notificationServices = notificationServices;
    }

    @GetMapping("/Notifications")
    public ResponseEntity<?> getNotifications(){
        return ResponseEntity.ok ( notificationServices.getAll () );
    }

    @GetMapping("/Notification/{notificationId}")
    public ResponseEntity<?> getNotification(@PathVariable Long notificationId){
        return ResponseEntity.ok ( notificationServices.getById ( notificationId ) );
    }

    @PostMapping("/Notification")
    public ResponseEntity<?> saveNotification(@RequestBody Notification notification){
        return ResponseEntity.ok ( notificationServices.create ( notification ) );
    }

    @PutMapping("/Notification")
    public ResponseEntity<?> updateNotification(@RequestBody Notification notification){
        return ResponseEntity.ok ( notificationServices.update ( notification ) );
    }

    @DeleteMapping("/Notification/{notificationId}")
    public void deleteNotification(@PathVariable Long notificationId){
        notificationServices.delete ( notificationId );
    }

}
