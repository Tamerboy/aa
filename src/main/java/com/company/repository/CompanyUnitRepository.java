package com.company.repository;

import com.company.model.CompanyUnit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyUnitRepository extends CrudRepository<CompanyUnit, Long> {

    List<CompanyUnit> findAll();
}
