package com.company.repository;

import com.company.model.Tempfiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface TempfilesRepository extends CrudRepository<Tempfiles,Long> {

    List<Tempfiles> findAll();
}
