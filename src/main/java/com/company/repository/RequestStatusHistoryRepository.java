package com.company.repository;

import com.company.model.RequestStatusHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestStatusHistoryRepository extends CrudRepository<RequestStatusHistory,Long> {

    List<RequestStatusHistory> findAll();
}
