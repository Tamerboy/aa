package com.company.repository;

import com.company.model.CaseIndex;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaseIndexRepository extends CrudRepository<CaseIndex,Long> {

    List<CaseIndex> findAll();
}
