package com.company.services;

import com.company.model.SearchKeyRouting;
import com.company.repository.SearchKeyRoutingRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchKeyRoutingService {

    private final SearchKeyRoutingRepository searchKeyRoutingRepository;

    public SearchKeyRoutingService(SearchKeyRoutingRepository searchKeyRoutingRepository) {
        this.searchKeyRoutingRepository = searchKeyRoutingRepository;
    }

    public List<SearchKeyRouting> getAll(){
        return searchKeyRoutingRepository.findAll ();
    }

    public SearchKeyRouting getById(Long id){
        return searchKeyRoutingRepository.findById ( id ).orElse ( null );
    }

    public SearchKeyRouting create(SearchKeyRouting searchKeyRouting){
        return searchKeyRoutingRepository.save ( searchKeyRouting );
    }

    public SearchKeyRouting update(SearchKeyRouting searchKeyRouting){
        return searchKeyRoutingRepository.save ( searchKeyRouting );
    }
    public void delete(Long id){
        searchKeyRoutingRepository.deleteById ( id );
    }
}
