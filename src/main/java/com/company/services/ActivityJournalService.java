package com.company.services;

import com.company.repository.ActivityJournalRepository;
import com.company.model.ActivityJournal;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityJournalService {

    private final ActivityJournalRepository activityJournalRepository;

    public ActivityJournalService(ActivityJournalRepository activityJournalRepository) {
        this.activityJournalRepository = activityJournalRepository;
    }

    public List<ActivityJournal> getAll(){
        return activityJournalRepository.findAll ();
    }

    public ActivityJournal getById(Long id){
        return activityJournalRepository.findById ( id ).orElse ( null );
    }

    public ActivityJournal create(ActivityJournal activityJournal){
        return activityJournalRepository.save ( activityJournal );
    }

    public ActivityJournal update(ActivityJournal activityJournal){
        return activityJournalRepository.save ( activityJournal );
    }

    public void delete(Long id){
        activityJournalRepository.deleteById ( id );
    }
}
