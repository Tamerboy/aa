package com.company.services;

import com.company.repository.TempfilesRepository;
import com.company.model.Tempfiles;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TempfilesService {

    private final TempfilesRepository tempfilesRepository;

    public TempfilesService(TempfilesRepository tempfilesRepository) {
        this.tempfilesRepository = tempfilesRepository;
    }

    public List<Tempfiles> getAll(){
        return tempfilesRepository.findAll ();
    }

    public Tempfiles getById(Long id){
        return tempfilesRepository.findById (  id).orElse ( null );
    }

    public Tempfiles create(Tempfiles tempfiles){
        return tempfilesRepository.save ( tempfiles );
    }

    public Tempfiles update(Tempfiles tempfiles){
        return tempfilesRepository.save ( tempfiles );
    }

    public void delete(Long id){
        tempfilesRepository.deleteById ( id );
    }
}
