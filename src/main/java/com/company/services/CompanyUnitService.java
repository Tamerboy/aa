package com.company.services;

import com.company.model.CompanyUnit;
import com.company.repository.CompanyUnitRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyUnitService {

    private final CompanyUnitRepository companyUnitRepository;

    public CompanyUnitService(CompanyUnitRepository companyUnitRepository) {
        this.companyUnitRepository = companyUnitRepository;
    }

    public List<CompanyUnit> getAll(){
        return companyUnitRepository.findAll ();
    }

    public CompanyUnit getById(Long id){
        return companyUnitRepository.findById (  id).orElse ( null );
    }

    public CompanyUnit create(CompanyUnit companyUnit){
        return companyUnitRepository.save ( companyUnit );
    }

    public CompanyUnit update(CompanyUnit companyUnit){
        return companyUnitRepository.save ( companyUnit );
    }

    public void delete(Long id){
        companyUnitRepository.deleteById ( id );
    }

}
