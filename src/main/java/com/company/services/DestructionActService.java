package com.company.services;

import com.company.repository.DestructionActRepository;
import com.company.model.DestructionAct;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DestructionActService {

    private final DestructionActRepository destructionActRepository;

    public DestructionActService(DestructionActRepository destructionActRepository) {
        this.destructionActRepository = destructionActRepository;
    }

    public List<DestructionAct> getAll(){
        return destructionActRepository.findAll ();
    }

    public DestructionAct getById(Long id){
        return destructionActRepository.findById ( id ).orElse ( null );
    }

    public DestructionAct create(DestructionAct destructionAct){
        return destructionActRepository.save ( destructionAct );
    }

    public DestructionAct update(DestructionAct destructionAct){
        return destructionActRepository.save ( destructionAct );
    }

    public void delete(Long id){
        destructionActRepository.deleteById ( id );
    }

}
