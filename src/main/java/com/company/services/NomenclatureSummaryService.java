package com.company.services;

import com.company.model.NomenclatureSummary;
import com.company.repository.NomenclatureSummaryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NomenclatureSummaryService {

    private final NomenclatureSummaryRepository nomenclatureSummaryRepository;

    public NomenclatureSummaryService(NomenclatureSummaryRepository nomenclatureSummaryRepository) {
        this.nomenclatureSummaryRepository = nomenclatureSummaryRepository;
    }

    public List<NomenclatureSummary> getAll(){
        return nomenclatureSummaryRepository.findAll ();
    }

    public NomenclatureSummary getById(Long id){
        return nomenclatureSummaryRepository.findById ( id ).orElse ( null );
    }

    public NomenclatureSummary create(NomenclatureSummary nomenclatureSummary){
        return nomenclatureSummaryRepository.save ( nomenclatureSummary );
    }

    public NomenclatureSummary update(NomenclatureSummary nomenclatureSummary){
        return nomenclatureSummaryRepository.save ( nomenclatureSummary );
    }

    public void delete(Long id ){
        nomenclatureSummaryRepository.deleteById ( id );
    }
}
