package com.company.services;

import com.company.model.Location;
import com.company.repository.LocationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService {

    private final LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public List<Location> getAll(){
        return locationRepository.findAll ();
    }

    public Location getById(Long id){
        return locationRepository.findById ( id ).orElse ( null );
    }

    public Location create(Location location){
        return locationRepository.save ( location );
    }

    public Location update(Location location){
        return locationRepository.save ( location );
    }

    public void delete(Long id){
        locationRepository.deleteById ( id );
    }
}
