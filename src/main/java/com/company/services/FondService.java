package com.company.services;

import com.company.model.Fond;
import com.company.repository.FondRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FondService {

    private final FondRepository fondRepository;

    public FondService(FondRepository fondRepository) {
        this.fondRepository = fondRepository;
    }

    public List<Fond> getAll(){
        return fondRepository.findAll ();
    }

    public Fond getById(Long id){
        return fondRepository.findById ( id ).orElse ( null );
    }

    public Fond create(Fond fond){
        return fondRepository.save ( fond );
    }

    public Fond update(Fond fond){
        return fondRepository.save ( fond );
    }

    public void delete(Long id){
        fondRepository.deleteById ( id );
    }
}
