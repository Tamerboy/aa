package com.company.services;

import com.company.repository.RequestRepository;
import com.company.model.Request;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestService {

    private final RequestRepository requestRepository;

    public RequestService(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public List<Request> getAll(){
        return requestRepository.findAll ();
    }

    public Request getById(Long id){
        return requestRepository.findById ( id ).orElse ( null );
    }

    public Request create(Request request){
        return requestRepository.save ( request );
    }

    public Request update(Request request){
        return requestRepository.save ( request );
    }

    public void delete(Long id){
        requestRepository.deleteById ( id );
    }
}
