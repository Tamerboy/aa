package com.company.services;

import com.company.repository.RequestStatusHistoryRepository;
import com.company.model.RequestStatusHistory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestStatusHistoryService {

    private final RequestStatusHistoryRepository requestStatusHistoryRepository;

    public RequestStatusHistoryService(RequestStatusHistoryRepository requestStatusHistoryRepository) {
        this.requestStatusHistoryRepository = requestStatusHistoryRepository;
    }

    public List<RequestStatusHistory> getAll(){
        return requestStatusHistoryRepository.findAll ();
    }

    public RequestStatusHistory getById(Long id){
        return requestStatusHistoryRepository.findById ( id ).orElse ( null );
    }

    public RequestStatusHistory create(RequestStatusHistory requestStatusHistory){
        return requestStatusHistoryRepository.save ( requestStatusHistory );
    }

    public RequestStatusHistory update(RequestStatusHistory requestStatusHistory){
        return requestStatusHistoryRepository.save ( requestStatusHistory );
    }

    public void delete(Long id){
        requestStatusHistoryRepository.deleteById ( id );
    }


}
