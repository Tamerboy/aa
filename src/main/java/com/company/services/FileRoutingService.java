package com.company.services;

import com.company.repository.FileRoutingRepository;
import com.company.model.FileRouting;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileRoutingService {

    private final FileRoutingRepository fileRoutingRepository;

    public FileRoutingService(FileRoutingRepository fileRoutingRepository) {
        this.fileRoutingRepository = fileRoutingRepository;
    }

    public List<FileRouting> getAll(){
        return fileRoutingRepository.findAll ();
    }

    public FileRouting getById(Long id){
        return fileRoutingRepository.findById ( id ).orElse ( null );
    }

    public FileRouting create(FileRouting fileRouting){
        return fileRoutingRepository.save ( fileRouting );
    }

    public FileRouting update(FileRouting fileRouting){
        return fileRoutingRepository.save ( fileRouting );
    }

    public void delete(Long id){
        fileRoutingRepository.deleteById ( id );
    }
}
