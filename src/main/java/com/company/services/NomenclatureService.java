package com.company.services;

import com.company.repository.NomenclatureRepository;
import com.company.model.Nomenclature;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NomenclatureService {

    private final NomenclatureRepository nomenclatureRepository;

    public NomenclatureService(NomenclatureRepository nomenclatureRepository) {
        this.nomenclatureRepository = nomenclatureRepository;
    }

    public List<Nomenclature> getAll(){
        return nomenclatureRepository.findAll ();
    }

    public Nomenclature getById(Long id){
        return nomenclatureRepository.findById ( id ).orElse ( null );
    }

    public Nomenclature create(Nomenclature nomenclature){
        return nomenclatureRepository.save ( nomenclature );
    }

    public Nomenclature update(Nomenclature nomenclature){
        return nomenclatureRepository.save ( nomenclature );
    }

    public void delete(Long id){
        nomenclatureRepository.deleteById ( id );
    }
}
